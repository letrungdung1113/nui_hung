<?php
  require_once ('database/connect.php');
?>
<?php
require_once ('header_page.php')
?>
      <!-- row -->
     
    </div> 
      </div>
        <!-- featured products -->
        <div class="small-container">
          <div class="row row-2 ">
              <h2> Tất cả sản phẩm </h2>
              <select name="" id="">
                  <option value="">Lọc</option>
              </select>
          </div>
          <div class="row"> 
                  <!-- Xu ly lay san pham -->
        <?php
        $stmt = $objConn->prepare("SELECT * FROM product");
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $db = $stmt->fetchAll();
        ?>
        <?php foreach ($db as $item) {
        ?>
            <div class="col-4">
                <div class="center" style="text-align: center;">
                    <!-- anh san pham -->
                    <img src="<?= $item['thumbnail'] ?>" alt="">
                    <!-- tieu de san pham -->
                    <h4> <?= $item['title'] ?> </h4>
                    <!-- gia san pham -->
                    <p> <?= $item['price'] ?> </p>
                    <!-- link chi tiet san pham -->
                    <a href="detail-product.php?id=<?= $item['id'] ?>" class="btn btn-danger"> Xem thêm </a>
                </div>

            </div>
        <?php
        }
        ?>
           
         
      </div>
      <!-- page btn -->
      <div class="page-btn">
          <span>1</span>
          <span>2</span>
          <span>3</span>
          <span>4</span>
          <span>5</span>
          <span>&#8594;</span>
      </div>
     
      </div>
         <!-- footer -->
         <?php require_once('footer_page.php') ?>
  </body>
</html>
